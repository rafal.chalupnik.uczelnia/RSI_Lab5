﻿using System;

namespace WcfClient
{
    public class Program
    {
        public static void Main(string[] _args)
        {
            var client = new WcfServiceClient();
            const int n = 10;
            Console.WriteLine($"Client started, calling Silnia({n})");

            var result = client.Silnia(n);
            Console.WriteLine($"Result: {result}. Press Enter to exit.");

            Console.ReadLine();
            client.Close();
        }
    }
}
