﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace WcfRestApplication
{
    [ServiceContract]
    public interface IRestService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/Books")]
        List<Book> GetAllBooks();

        [OperationContract]
        [WebGet(UriTemplate = "/json/Books")]
        List<Book> GetAllBooksJson();

        [OperationContract]
        [WebGet(UriTemplate = "/Books/{_id}", ResponseFormat = WebMessageFormat.Xml)]
        Book GetBookById(string _id);

        [OperationContract]
        [WebGet(UriTemplate = "/json/Books/{_id}", ResponseFormat = WebMessageFormat.Json)]
        Book GetBookByIdJson(string _id);

        [OperationContract]
        [WebInvoke(UriTemplate = "/Books/{_id}", Method = "PUT", RequestFormat = WebMessageFormat.Xml)]
        bool UpdateBook(string _id, Book _book);

        [OperationContract]
        [WebInvoke(UriTemplate = "/json/Books/{_id}", Method = "PUT", RequestFormat = WebMessageFormat.Json)]
        bool UpdateBookJson(string _id, Book _book);
    }

    
    [DataContract]
    public class Book
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public decimal Price { get; set; }
    }
}
