﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace WcfRestApplication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class RestService : IRestService
    {
        private readonly List<Book> books_ = new List<Book>()
        {
            new Book {Id = 1, Title = "Poradnik", Price = 12.00m},
            new Book {Id = 2, Title = "Powieść", Price = 35.00m},
            new Book {Id = 3, Title = "Kryminał", Price = 42.33m}
        };

        public List<Book> GetAllBooks()
        {
            return books_;
        }

        public List<Book> GetAllBooksJson()
        {
            return GetAllBooks();
        }

        public Book GetBookById(string _id)
        {
            return books_.FirstOrDefault(_book => _book.Id.ToString() == _id);
        }

        public Book GetBookByIdJson(string _id)
        {
            return GetBookById(_id);
        }

        public bool UpdateBook(string _id, Book _book)
        {
            if (string.IsNullOrEmpty(_id) || _book == null)
                return false;

            var index = books_.FindIndex(_b => _b.Id.ToString() == _id);

            if (index == -1)
                return false;

            books_[index] = _book;
            return true;
        }

        public bool UpdateBookJson(string _id, Book _book)
        {
            return UpdateBook(_id, _book);
        }
    }
}
