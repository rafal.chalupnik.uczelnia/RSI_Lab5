﻿using System.ServiceModel;

namespace WcfApplication
{
    [ServiceContract]
    public interface IWcfService
    {
        [OperationContract]
        int Silnia(int _n);
    }
}
