﻿namespace WcfApplication
{
    public class WcfService : IWcfService
    {
        public int Silnia(int _n)
        {
            if (_n <= 1)
                return 1;

            return _n * Silnia(_n - 1);
        }
    }
}
