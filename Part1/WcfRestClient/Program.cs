﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace WcfRestClient
{
    public class Program
    {
        public static void Main(string[] _args)
        {
            try
            {
                Console.WriteLine("Podaj format (xml lub json):");
                var format = Console.ReadLine();
                Console.WriteLine("Podaj metode (GET lub PUT lub ...):");
                var method = Console.ReadLine();
                Console.WriteLine("Podaj URI:");
                var uri = Console.ReadLine();
                var httpRequest = WebRequest.Create(uri) as HttpWebRequest;
                httpRequest.KeepAlive = false;
                httpRequest.Method = method.ToUpper();
                if (format == "xml")
                    httpRequest.ContentType = "text/xml";
                else if (format == "json")
                    httpRequest.ContentType = "application/json";
                else
                {
                    Console.WriteLine("Podales zle dane!");
                    return;
                }
                    
                switch (method.ToUpper())
                {
                    case "GET":
                        break;
                    case "PUT":
                        Console.WriteLine("Wklej zawartosc XML-a lub JSON-a w jednej linii");
                        var dane = Console.ReadLine();
                        byte[] bufor = Encoding.UTF8.GetBytes(dane);
                        httpRequest.ContentLength = bufor.Length;
                        Stream postData = httpRequest.GetRequestStream();
                        postData.Write(bufor, 0, bufor.Length);
                        postData.Close();
                        break;
                }
                
                var httpResponse = httpRequest.GetResponse() as HttpWebResponse;
                var encoding = Encoding.GetEncoding(1252);
                var responseStream =
                    new StreamReader(httpResponse.GetResponseStream(), encoding);
                var responseString = responseStream.ReadToEnd();
                responseStream.Close();
                httpResponse.Close();
                Console.WriteLine(responseString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
    }
}
