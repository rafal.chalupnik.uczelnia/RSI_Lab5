﻿namespace StudentRestClient
{
    public class Student
    {
        public int Index { get; set; }
        
        public string Surname { get; set; }
        
        public string FirstName { get; set; }
        
        public string Place { get; set; }
        
        public string Pesel { get; set; }
    }
}