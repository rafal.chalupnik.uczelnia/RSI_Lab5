﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;

namespace StudentRestClient
{
    public partial class MainWindow
    {
        private const string BaseAddress = "http://localhost/StudentService.svc";

        private static string GetJson(string _uri)
        {
            var request = WebRequest.Create(_uri) as HttpWebRequest;
            request.KeepAlive = false;
            request.Method = "GET";
            request.ContentType = "application/json";

            var response = request.GetResponse() as HttpWebResponse;
            return new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
        }

        private static bool ExecuteJson(string _uri, string _method, string _json = null)
        {
            var request = WebRequest.Create(_uri) as HttpWebRequest;
            request.KeepAlive = false;
            request.Method = _method;
            request.ContentType = "application/json";

            if (_json != null)
            {
                var requestStream = request.GetRequestStream();
                var jsonBuffer = Encoding.UTF8.GetBytes(_json);
                requestStream.Write(jsonBuffer, 0, jsonBuffer.Length);
                requestStream.Close();
            }
            
            var response = request.GetResponse();
            return JsonConvert.DeserializeObject<bool>(new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1252)).ReadToEnd());
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object _sender, RoutedEventArgs _args)
        {
            var indexCorrect = int.TryParse(IndexNumberTextBox.Text, out var index);

            if (!indexCorrect)
            {
                MessageBox.Show("Indeks musi składać się z cyfr!");
                return;
            }

            var student = new Student()
            {
                Index = index,
                Surname = SurnameTextBox.Text,
                FirstName = FirstNameTextBox.Text,
                Place = PlaceTextBox.Text,
                Pesel = PeselTextBox.Text
            };

            var result = ExecuteJson($"{BaseAddress}/students/{index}", "PUT", JsonConvert.SerializeObject(student));

            MessageBox.Show(result ? "Dodano pomyślnie!" : "Nie udało się dodać studenta.");
        }

        private void DeleteButton_Click(object _sender, RoutedEventArgs _args)
        {
            var indexCorrect = int.TryParse(IndexNumberTextBox.Text, out var index);

            if (!indexCorrect)
            {
                MessageBox.Show("Indeks musi składać się z cyfr!");
                return;
            }

            var student = new Student()
            {
                Index = index,
                Surname = SurnameTextBox.Text,
                FirstName = FirstNameTextBox.Text,
                Place = PlaceTextBox.Text,
                Pesel = PeselTextBox.Text
            };

            var result = ExecuteJson($"{BaseAddress}/students/{index}", "DELETE");

            MessageBox.Show(result ? "Usunięto pomyślnie!" : "Nie udało się usunąć studenta.");
        }

        private void EditButton_Click(object _sender, RoutedEventArgs _args)
        {
            var indexCorrect = int.TryParse(IndexNumberTextBox.Text, out var index);

            if (!indexCorrect)
            {
                MessageBox.Show("Indeks musi składać się z cyfr!");
                return;
            }

            var student = new Student()
            {
                Index = index,
                Surname = SurnameTextBox.Text,
                FirstName = FirstNameTextBox.Text,
                Place = PlaceTextBox.Text,
                Pesel = PeselTextBox.Text
            };

            var result = ExecuteJson($"{BaseAddress}/students/{index}", "POST", JsonConvert.SerializeObject(student));

            MessageBox.Show(result ? "Zedytowano pomyślnie!" : "Nie udało się zedytować studenta.");
        }

        private void NewButton_Click(object _sender, RoutedEventArgs _args)
        {
            IndexNumberTextBox.Clear();
            SurnameTextBox.Clear();
            FirstNameTextBox.Clear();
            PlaceTextBox.Clear();
            PeselTextBox.Clear();
        }

        private void RefreshButton_Click(object _sender, RoutedEventArgs _args)
        {
            try
            {
                StudentsListView.SelectedIndex = -1;

                var students = JsonConvert.DeserializeObject<List<string>>(GetJson($"{BaseAddress}/students"));
                StudentsListView.Items.Clear();

                foreach (var student in students)
                    StudentsListView.Items.Add(student);
            }
            catch (Exception)
            {
            }
        }

        private void SearchButton_Click(object _sender, RoutedEventArgs _args)
        {
            try
            {
                var students = JsonConvert.DeserializeObject<List<string>>(GetJson($"{BaseAddress}/students/query/{QueryTextBox.Text}"));
                StudentsListView.Items.Clear();

                foreach (var student in students)
                    StudentsListView.Items.Add(student);
            }
            catch (Exception)
            {
            }
        }

        private void StudentsListView_OnSelectionChanged(object _sender, SelectionChangedEventArgs _e)
        {
            var selectedItem = StudentsListView.SelectedValue as string;

            var student = JsonConvert.DeserializeObject<Student>(GetJson($"{BaseAddress}/students/{selectedItem}"));

            if (student != null)
            {
                IndexNumberTextBox.Text = student.Index.ToString();
                SurnameTextBox.Text = student.Surname;
                FirstNameTextBox.Text = student.FirstName;
                PlaceTextBox.Text = student.Place;
                PeselTextBox.Text = student.Pesel;
            }
            else
                MessageBox.Show("Nie udało się pobrać danych studenta.");
        }
    }
}