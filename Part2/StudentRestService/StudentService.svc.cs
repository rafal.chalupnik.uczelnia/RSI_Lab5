﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace StudentRestService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class StudentService : IStudentService
    {
        private readonly List<Student> students_ = new List<Student>();
        
        public bool AddStudent(string _indexString, Student _student)
        {
            if (string.IsNullOrEmpty(_indexString) 
                || _student == null 
                || !int.TryParse(_indexString, out var index)
                || index != _student.Index)
                return false;

            var listIndex = students_.FindIndex(_s => _s.Index == _student.Index);

            if (listIndex != -1)
                return false;

            students_.Add(_student);
            return true;
        }

        public bool DeleteStudent(string _indexString)
        {
            if (string.IsNullOrEmpty(_indexString) || !int.TryParse(_indexString, out var index))
                return false;

            var listIndex = students_.FindIndex(_s => _s.Index == index);
            if (listIndex == -1)
                return false;

            students_.RemoveAt(listIndex);
            return true;
        }

        public bool EditStudent(string _indexString, Student _student)
        {
            if (string.IsNullOrEmpty(_indexString)
                || _student == null
                || !int.TryParse(_indexString, out var index)
                || index != _student.Index)
                return false;

            var listIndex = students_.FindIndex(_s => _s.Index == index);

            if (listIndex == -1)
                return false;

            students_[listIndex] = _student;
            return true;
        }

        public List<string> FindStudents(string _query)
        {
            _query = _query.ToLower();

            var surnameResults = students_.Where(_s => _s.Surname?.ToLower().Contains(_query) ?? false);
            var firstNameResults = students_.Where(_s => _s.FirstName?.ToLower().Contains(_query) ?? false);
            var placeResults = students_.Where(_s => _s.Place?.ToLower().Contains(_query) ?? false);
            var peselResults = students_.Where(_s => _s.Pesel?.ToLower().Contains(_query) ?? false);

            return surnameResults.Concat(firstNameResults).Concat(placeResults).Concat(peselResults).Select(_s => _s.Index.ToString()).ToList();
        }

        public List<string> GetAllStudents()
        {
            return students_.Select(_s => _s.Index.ToString()).ToList();
        }
        
        public Student GetStudentByIndex(string _indexString)
        {
            if (string.IsNullOrEmpty(_indexString) || !int.TryParse(_indexString, out var index))
                return null;

            return students_.FirstOrDefault(_s => _s.Index == index);
        }
    }
}