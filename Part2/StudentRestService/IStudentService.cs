﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace StudentRestService
{
    [ServiceContract]
    public interface IStudentService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "students/{_indexString}", Method = "PUT", ResponseFormat = WebMessageFormat.Json)]
        bool AddStudent(string _indexString, Student _student);

        [OperationContract]
        [WebInvoke(UriTemplate = "/students/{_indexString}", Method = "DELETE", ResponseFormat = WebMessageFormat.Json)]
        bool DeleteStudent(string _indexString);

        [OperationContract]
        [WebInvoke(UriTemplate = "/students/{_indexString}", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        bool EditStudent(string _indexString, Student _student);

        [OperationContract]
        [WebGet(UriTemplate = "/students/query/{_query}", ResponseFormat = WebMessageFormat.Json)]
        List<string> FindStudents(string _query);
        
        [OperationContract]
        [WebGet(UriTemplate = "/students", ResponseFormat = WebMessageFormat.Json)]
        List<string> GetAllStudents();

        [OperationContract]
        [WebGet(UriTemplate = "/students/{_indexString}", ResponseFormat = WebMessageFormat.Json)]
        Student GetStudentByIndex(string _indexString);
    }

    [DataContract]
    public class Student
    {
        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Place { get; set; }

        [DataMember]
        public string Pesel { get; set; }
    }
}
